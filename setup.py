from setuptools import setup, find_packages
import os

setup(
        name = "interact",
        packages = ['interact'],
        version = "0.2",
        description = "interact for api",
        long_description='interact is interact',
        author = "Antoine Nguyen",
        author_email = "tonio@ngyn.org",
        url = "http://bitbucket.org/iddqd1/cmsplugin-bootstrap-carousel",
        license = "BSD",
        keywords = ["coworks"],
        classifiers = [
                    "Programming Language :: Python",
                    "Environment :: Web Environment",
                    "Development Status :: 4 - Beta",
                    "Intended Audience :: Developers",
                    "License :: OSI Approved :: BSD License",
                    "Operating System :: OS Independent",
                    ],
        include_package_data = True,
        zip_safe = True,
        )
