import interact.zk_helper
import interact.zmq_helper
import threading
import zookeeper
import unittest

class ContextTest(unittest.TestCase):
    service_name = 'x'
    zk_host_port = 'y'
    service_host_port = 'z'
    request_schema = 'a'
    response_schema = 'b'

    def test_request_service(self):
        from interact.context import RequestServiceContext
        context = RequestServiceContext(self.service_name, self.zk_host_port, self.request_schema, self.response_schema)
        self.assertTrue(self.service_name == context.service_name)
        self.assertTrue(self.zk_host_port == context.zk_host_port)
        self.assertTrue(self.request_schema == context.request_schema)
        self.assertTrue(self.response_schema == context.response_schema)

    def test_response_service(self):
        from interact.context import ResponseServiceContext
        context = ResponseServiceContext(self.service_name, self.service_host_port, self.zk_host_port, self.request_schema, self.response_schema)
        self.assertTrue(self.service_name == context.service_name)
        self.assertTrue(self.service_host_port == context.service_host_port)
        self.assertTrue(self.zk_host_port == context.zk_host_port)
        self.assertTrue(self.request_schema == context.request_schema)
        self.assertTrue(self.response_schema == context.response_schema)

    def test_publish_service(self):
        from interact.context import PublishServiceContext
        context = PublishServiceContext(self.service_name, self.service_host_port, self.zk_host_port, self.request_schema)
        self.assertTrue(self.service_name == context.service_name)
        self.assertTrue(self.service_host_port == context.service_host_port)
        self.assertTrue(self.zk_host_port == context.zk_host_port)
        self.assertTrue(self.request_schema == context.notification_schema)

    def test_subscription_service(self):
        from interact.context import SubscriptionServiceContext
        context = SubscriptionServiceContext(self.service_name, self.zk_host_port, self.request_schema)
        self.assertTrue(self.service_name == context.service_name)
        self.assertTrue(self.zk_host_port == context.zk_host_port)
        self.assertTrue(self.request_schema == context.notification_schema)

from interact.json_helper import JsonValidator
from validictory.validator import ValidationError

class JsonValidatorTest(unittest.TestCase):
    schema = '{"type":"object", "properties":{"banana":{"type":"string"}}}'
    valid_data = '{"banana":"this is a string"}'
    invalid_data = '{"banana":42}'

    def test_request(self):
        validator = JsonValidator(request_schema = self.schema)
        validator.validate_request(self.valid_data)
        self.assertRaises(ValidationError, validator.validate_request, self.invalid_data)

    def test_response(self):
        validator = JsonValidator(response_schema = self.schema)
        validator.validate_response(self.valid_data)
        self.assertRaises(ValidationError, validator.validate_response, self.invalid_data)

    def test_notification(self):
        validator = JsonValidator(notification_schema = self.schema)
        validator.validate_notification(self.valid_data)
        self.assertRaises(ValidationError, validator.validate_notification, self.invalid_data)


class ZkMock(object):
    def __init__(self):
        self.tree = dict()
        self.watchers = dict()
        self.create_count = 0
        self.get_children_count = 0
        self.delete_count = 0
        self.init_count = 0

    def create(self, zk_handle, node, data, acl, mode):
        self.create_count += 1
        self.maybe_raise_exception(self.create_count)
        nodes = node.split('/')
        tree = self.tree
        created = False
        for node in nodes:
            leaf = tree
            if node in tree:
                tree = tree[node]
            else:
                created = True
                tree[node] = dict()
                tree = tree[node]
                print "added %s" % node
        if not created:
            raise zookeeper.NodeExistsException
        if id(leaf) in self.watchers.keys():
            #watchers are removed after they are called
            #the watcher func also add a watcher, so this inhibits an infinite loop
            watchers = self.watchers.pop(id(leaf)) 
            for watcher in watchers:
                if watcher is not None:
                    watcher(None, None, None, None)

    def get_children(self, zk_handle, node, watcher):
        self.get_children_count += 1
        self.maybe_raise_exception(self.get_children_count)
        nodes = node.split('/')
        tree = self.tree
        for node in nodes:
            tree = tree[node]
        if id(tree) in self.watchers.keys():
            self.watchers[id(tree)].append(watcher)
        else:
            self.watchers[id(tree)] = [watcher]
        print "get returns %s" % tree.keys()
        return tree.keys()

    def adelete(self, zk_handle, node):
        self.delete_count += 1
        self.maybe_raise_exception(self.delete_count)
        nodes = node.split('/')
        leaf = nodes.pop()
        tree = self.tree
        for node in nodes:
            tree = tree[node]
        tree.pop(leaf)
        if id(tree) in self.watchers.keys():
            #watchers are removed after they are called
            #the watcher func also add a watcher, so this inhibits an infinite loop
            watchers = self.watchers.pop(id(tree)) 
            for watcher in watchers:
                watcher(None, None, None, None)

    def init(self, quorum, connected_callback, timeout):
        self.init_count += 1
        self.maybe_raise_exception(self.init_count)
        threading.Thread(target=connected_callback, args=[None, None, None, None], group=None).start()
        return None

    def maybe_raise_exception(self, counter):
        if counter % 3 == 0:
            if counter % 6 == 0:
                raise zookeeper.ConnectionLossException
            else:
                raise zookeeper.OperationTimeoutException

    def close(self, handle):
        pass

class ZkHelperTest(unittest.TestCase):
    def test(self):
        mock = ZkMock()
        interact.zk_helper.set_zk(mock)
        actually_added1 = []
        actually_removed1 = []
        def added1(list): actually_added1.extend(list)
        def removed1(list): actually_removed1.extend(list)
        actually_added2 = []
        actually_removed2 = []
        def added2(list): actually_added2.extend(list)
        def removed2(list): actually_removed2.extend(list)
        
        p1 = interact.zk_helper.ZkPublish('', 'service_name', 'host_port1')
        p1.start()
        p2 = interact.zk_helper.ZkPublish('', 'service_name', 'host_port2')
        p2.start()
        s1 = interact.zk_helper.ZkSubscribe('', 'service_name', added1, removed1)
        s1.start()
        p1.stop()
        p3 = interact.zk_helper.ZkPublish('', 'service_name', 'host_port3')
        p3.start()
        p2.stop()
        s2 = interact.zk_helper.ZkSubscribe('', 'service_name', added2, removed2)
        s2.start()
        p3.stop()
        s1.stop()
        s2.stop()
        
        passed = False
        if len(actually_added1) == 3 and 'host_port1' in actually_added1 and 'host_port2' in actually_added1 and 'host_port3' in actually_added1:
            passed = True
        if not passed:
            self.fail('expected [host_port1, host_port2, host_port3], got %s' % actually_added1)
        passed = False
        if len(actually_added2) == 1 and 'host_port3' in actually_added2:
            passed = True
        if not passed:
            self.fail('expected [host_port3], got %s' % actually_added2)
        passed = False
        if len(actually_removed1) == 3 and 'host_port1' in actually_removed1 and 'host_port2' in actually_removed1 and 'host_port3' in actually_removed1:
            passed = True
        if not passed:
            self.fail('expected [host_port1, host_port2, host_port3], got %s' % actually_removed1)
        passed = False
        if len(actually_removed2) == 1 and 'host_port3' in actually_removed2:
            passed = True
        if not passed:
            self.fail('expected [host_port3], got %s' % actually_removed2)




import interact.zmq_helper
import time
import logging
import zmq
logging.basicConfig()

class ZmqMock(object):
    def __init__(self):
        self.context = Context()

    def Context(self):
        return self.context

    def Poller(self):
        return Poller()

class Context(object):
    def __init__(self):
        self.servers = dict()
        self.clients = dict()
    def socket(self, type):
        return Socket(self, type)
    def term(self):
        self.__init__()
    def bind(self, socket, endpoint):
        self.servers[endpoint] = socket
    def connect(self, socket, endpoint):
        if endpoint in self.clients.keys():
            self.clients[endpoint].append(socket)
        else:
            self.clients[endpoint] = [socket]
    def transfer(self, endpoint, data, server_to_client):
        data.reverse()
        if server_to_client:
            for client in self.clients.get(endpoint):
                for chunk in data:
                    client.incoming_data.insert(0, chunk)
                    print 'transfered to %s %s %s %s' % (id(client), endpoint, chunk, server_to_client)
        else:
            for chunk in data:
                self.servers[endpoint].incoming_data.insert(0, chunk)
                print 'transfered to %s %s %s %s' % (id(self.servers[endpoint]), endpoint, chunk, server_to_client)

INTERNAL_ID = '____id'
class Socket(object):
    def __init__(self, context, type):
        self.context = context
        self.type = type
        self.server = False
        self.endpoint = None
        self.incoming_data = []
        self.outgoing_data = []
    def bind(self, endpoint):
        self.server = True
        self.endpoint = endpoint
        self.context.bind(self, endpoint)
        print 'socket %s bound to %s' % (id(self), endpoint)
    def setsockopt(self, _a, _b):
        pass
    def connect(self, endpoint):
        self.endpoint = endpoint
        self.context.connect(self, endpoint)
        print 'socket %s connected to %s' % (id(self), endpoint)
    def close(self):
        pass
    def send(self, data, flags=0, copy=True):
        if flags != 0:
            self.outgoing_data.insert(0, data)
        else:
            self.outgoing_data.insert(0, data)
            outgoing_data = self.outgoing_data
            if self.type == zmq.ROUTER:
                outgoing_data.append(INTERNAL_ID)
            self.outgoing_data = []
            print 'send from %s to %s' % (id(self), self.endpoint)
            self.context.transfer(self.endpoint, outgoing_data, self.server)
    def recv(self):
        data = self.incoming_data.pop()
        if self.type == zmq.ROUTER and data == INTERNAL_ID:
            data = self.incoming_data.pop()
        return data

class Poller(object):
    def __init__(self):
        self.sockets = []
    def register(self, socket, type):
        self.sockets.append((socket, type))
        print 'poller registered %s %s, %s, %s' % (id(socket), socket.server, socket.endpoint, type)
    def poll(self, timeout):
        result = self.do_poll()
        if len(result) > 0:
            return result
        time.sleep(0.1)
        return self.do_poll()
    def do_poll(self):
        result = []
        for (socket, type) in self.sockets:
            if len(socket.incoming_data) > 0:
                result.append((socket, type))
        return result
        

class ZmqHelperTest(unittest.TestCase):
    def test_publish(self):
        mock = ZmqMock()
        interact.zmq_helper.set_zmq(mock)
        
        passed_wrapper = [False]
        service_endpoint = 'host_port'
        DATA = 'test data'
        
        p = interact.zmq_helper.ZmqPublishService(service_endpoint)
        p.start()
        def callback(data): passed_wrapper[0] = data == DATA
        s = interact.zmq_helper.ZmqSubscriptionService(service_endpoint, callback)
        s.start()
        time.sleep(1)
        p.publish(DATA)
        time.sleep(1)
        p.close()
        s.close()
        if not passed_wrapper[0]:
            raise Exception
        
    def test_request(self):
        mock = ZmqMock()
        interact.zmq_helper.set_zmq(mock)
        service_endpoint = 'host_port'
        DATA = 'test data'

        missing_response_wrapper = [False]
        def missing_response_callback(corr_id, response): missing_response_wrapper[0] = True
        req = interact.zmq_helper.ZmqRequestService(service_endpoint, missing_response_callback=missing_response_callback)
        req.start()
        print 'XXX started req'
        sleep_wrapper = [0]
        def on_request(request, on_response): 
            print 'XXX got request'
            time.sleep(sleep_wrapper[0])
            print 'XXX send response'
            on_response(DATA)
        res = interact.zmq_helper.ZmqResponseService(service_endpoint, on_request)
        res.start()
        print 'XXX started res'

        passed_wrapper = [False]
        def on_response(response):
            print 'XXX got response'
            passed_wrapper[0] = response == DATA
        def on_timeout(): pass
        print 'XXX sending request'
        req.request('request', on_response, on_timeout, request_timeout = 10)
        print 'XXX sent request'
        time.sleep(2)
        if not (passed_wrapper[0] or missing_response_wrapper[0]):
            raise Exception
        
        passed_wrapper = [False]
        def on_response2(response): pass
        def on_timeout2(): passed_wrapper[0] = True
        sleep_wrapper[0] = 2
        missing_response_wrapper[0] = False
        req.request('request', on_response2, on_timeout2, request_timeout = 1)
        time.sleep(4)
        print 'JONAS %s %s' % (passed_wrapper[0], missing_response_wrapper[0])
        if not (passed_wrapper[0] and missing_response_wrapper[0]):
            raise Exception
        
        time.sleep(2)
        req.close()
        res.close()

#===============================================================================
# class ServiceTest(unittest.TestCase):
#    def test(self):
#        zmq_mock = ZmqMock()
#        from interact import zmq_helper
#        zmq_helper.set_zmq(zmq_mock)
#        zk_mock = ZkMock()
#        from interact import zk_helper
#        zk_helper.set_zk(zk_mock)
#        import interact.service as service
#        service.set_zk(interact.zk_helper)
#        service.set_zmq(interact.zmq_helper)
#        from interact.service import RequestService
#        from interact.context import RequestServiceContext
#        req_context = RequestServiceContext(service_name='Testtest')
#        req_service = RequestService(req_context)
#        req_service.start()
#        got_timeout = [False]
#        def on_response():
#            self.fail("should not have gotten a response")
#        def on_timeout():
#            got_timeout[0] = True
#        req_service.request('', on_response, on_timeout, response_timeout=1)
#        self.assertTrue(got_timeout[0])
#===============================================================================

