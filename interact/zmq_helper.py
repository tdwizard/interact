'''
Created on Aug 23, 2011

@author: jonas
'''

from apscheduler.scheduler import Scheduler
import datetime
import logging
import Queue
import threading
import time
import uuid
import zmq.green as zmq

POLL_TIMEOUT = 1000


class ZmqRequestService(threading.Thread):
    def __init__(self, host_port, missing_response_callback = None):
        threading.Thread.__init__ (self, name="zmq.request.%s" % host_port)
        self.daemon = True
        self._host_port = host_port
        self._missing_response_callback = self._default_missing_response_callback
        if missing_response_callback:
            self._missing_response_callback = missing_response_callback
        self._close = False
        self._uuid = str(uuid.uuid1())
        self._context = get_zmq().Context()
        '''
        zmq sockets are not thread safe, which causes issues since we need to 
        read and write in parallel.
        Handling it by using an internal in-process socket for writes, making
        it possible for the main loop to handle both writes and reads.
        '''
        internal_send_socket = self._context.socket(zmq.PUB)
        internal_send_socket.bind('inproc://internal' + self._uuid)
        self._internal_sender = Queue.Queue()
        self._internal_sender.put(internal_send_socket)
        self._internal_recv_socket = self._context.socket(zmq.SUB)
        self._internal_recv_socket.setsockopt(zmq.SUBSCRIBE, "")
        self._internal_recv_socket.connect('inproc://internal' + self._uuid)
        self._service_socket = self._context.socket(zmq.DEALER)
        self._service_socket.connect('tcp://' + self._host_port)
        self.response_callbacks = dict()
        self._scheduler = Scheduler()
        self._scheduler.start()
        self._scheduled_response_timeout_triggers = dict()
        self._logger = logging.getLogger('interact.zmqrequest.' + host_port)

    def run(self):
        poll = get_zmq().Poller()
        poll.register(self._service_socket, zmq.POLLIN)
        poll.register(self._internal_recv_socket, zmq.POLLIN)
        while not self._close:
            sockets = dict(poll.poll(POLL_TIMEOUT))
            if self._service_socket in sockets:
                if sockets[self._service_socket] == zmq.POLLIN:
                    correlation_id = self._service_socket.recv()
                    response = self._service_socket.recv()
                    if self.response_callbacks.has_key(correlation_id):
                        response_callback = self.response_callbacks.pop(correlation_id)[0]
                        response_timeout_trigger = self._scheduled_response_timeout_triggers.pop(correlation_id)
                        self._scheduler.unschedule_job(response_timeout_trigger)
                        response_callback(response)
                    else:
                        self._missing_response_callback(correlation_id, response)
            if self._internal_recv_socket in sockets:
                if sockets[self._internal_recv_socket] == zmq.POLLIN:
                    correlation_id = self._internal_recv_socket.recv()
                    request = self._internal_recv_socket.recv()
                    self._service_socket.send(correlation_id, zmq.SNDMORE, copy = False)
                    self._service_socket.send(request, copy = False)

        self._internal_sender.get().close()
        self._internal_recv_socket.close()
        self._service_socket.close()
        self._scheduler.shutdown()

    def close(self):
        if not self._close:
            self._close = True
            time.sleep(POLL_TIMEOUT / 1000.0)
            try:
                self._service_socket.close()
            except:
                pass
            try:
                self._internal_recv_socket.close()
            except:
                pass

    def request(self, request, response_callback, timeout_callback, request_timeout = 10):
        correlation_id = str(uuid.uuid1())
        self.response_callbacks[correlation_id] = (response_callback, timeout_callback)
        timeout_time = datetime.datetime.fromtimestamp(time.time() + request_timeout)
        response_timeout_trigger = self._scheduler.add_date_job(
                self._check_for_timed_out_request, 
                timeout_time, 
                [correlation_id])
        self._scheduled_response_timeout_triggers[correlation_id] = response_timeout_trigger
        sender = self._internal_sender.get()
        try:
            sender.send(correlation_id, zmq.SNDMORE, copy = False)
            sender.send(request, copy = False)
        finally:
            self._internal_sender.put(sender)

    def _check_for_timed_out_request(self, correlation_id):
        if self.response_callbacks.has_key(correlation_id):
            timeout_callback = self.response_callbacks.pop(correlation_id)[1]
            self._scheduled_response_timeout_triggers.pop(correlation_id)
            timeout_callback()

    def _default_missing_response_callback(self, correlation_id, response):
        self._logger.warn("Missing response callback, corr_id=%s, response=%s" % (correlation_id, response))


class ZmqResponseService(threading.Thread):
    def __init__(self, host_port, on_message_callback):
        threading.Thread.__init__ (self, name="zmq.response.%s" % host_port)
        self.daemon = True
        self._host_port = host_port
        self._on_message_callback = on_message_callback
        self._close = False
        self._uuid = str(uuid.uuid1())
        '''
        zmq sockets are not thread safe, which causes issues since we need to 
        read and write in parallel.
        Handling it by using an internal in-process socket for writes, making
        it possible for the main loop to handle both writes and reads.
        '''
        self._context = get_zmq().Context()
        internal_send_socket = self._context.socket(zmq.PUB)
        internal_send_socket.bind('inproc://internal' + self._uuid)
        self._internal_sender = Queue.Queue()
        self._internal_sender.put(internal_send_socket)
        self._internal_recv_socket = self._context.socket(zmq.SUB)
        self._internal_recv_socket.setsockopt(zmq.SUBSCRIBE, "")
        self._internal_recv_socket.connect('inproc://internal' + self._uuid)
        self._logger = logging.getLogger('interact.zmqresponse.' + host_port)
        self._socket = None
        self._request_counter = Queue.Queue()

    def run(self):
        self._socket = self._context.socket(zmq.ROUTER)
        endpoint = 'tcp://' + self._host_port
        self._logger.info('binding to ' + endpoint)
        self._socket.bind(endpoint)
        poll = get_zmq().Poller()
        poll.register(self._socket, zmq.POLLIN)
        poll.register(self._internal_recv_socket, zmq.POLLIN)
        while not self._close:
            sockets = dict(poll.poll(POLL_TIMEOUT))
            if self._socket in sockets:
                if sockets[self._socket] == zmq.POLLIN:
                    _id = self._socket.recv()
                    correlation_id = self._socket.recv()
                    msg = self._socket.recv()
                    def response_callback(response, correlation_id=correlation_id, _id=_id):
                        sender = self._internal_sender.get()
                        try:
                            sender.send(_id, zmq.SNDMORE, copy = False)
                            sender.send(correlation_id, zmq.SNDMORE, copy = False)
                            sender.send(response, copy = False)
                        finally:
                            self._internal_sender.put(sender)
                    self._request_counter.put(correlation_id) # add ongoing req
                    self._on_message_callback(msg, response_callback)
            if self._internal_recv_socket in sockets:
                if sockets[self._internal_recv_socket] == zmq.POLLIN:
                    _id = self._internal_recv_socket.recv()
                    correlation_id = self._internal_recv_socket.recv()
                    request = self._internal_recv_socket.recv()
                    self._socket.send(_id, zmq.SNDMORE, copy = False)
                    self._socket.send(correlation_id, zmq.SNDMORE, copy = False)
                    self._socket.send(request, copy = False)
                    self._request_counter.get() # remove ongoing req

        self._internal_sender.get().close()
        self._logger.info('closed ' + endpoint)

    def close(self):
        if not self._close:
            MAX_WAIT_TIME = 10
            start = time.time()
            while self._request_counter.qsize() > 0:
                if time.time() < (start + MAX_WAIT_TIME):
                    time.sleep(0.1)
                else:
                    self._logger.warn('closing even though %s requests havent returned' % self._request_counter.qsize())
                    break
            self._close = True
            # break out of the event loop
            time.sleep(POLL_TIMEOUT / 1000.0)
            try:
                self._socket.close()
            except:
                pass
            try:
                self._internal_recv_socket.close()
            except:
                pass


class ZmqPublishService(object):
    def __init__(self, host_port):
        self._host_port = host_port
        self._context = get_zmq().Context()
        self._logger = logging.getLogger('interact.zmqpublish.' + self._host_port)

    def start(self):
        socket = self._context.socket(zmq.PUB)
        socket.bind('tcp://' + self._host_port)
        self._sender_wrapper = Queue.Queue()
        self._sender_wrapper.put(socket)
        self._logger.info('connected to tcp://' + self._host_port)

    def close(self):
        try:
            self._sender_wrapper.get().close()
        except Exception as e:
            self._logger.warn('error closing tcp://%s: %s' % (self._host_port, e))
        self._logger.info('closed tcp://' + self._host_port)

    def publish(self, data):
        sender = self._sender_wrapper.get()
        try:
            sender.send(data, copy = False)
        finally:
            self._sender_wrapper.put(sender)


class ZmqSubscriptionService(threading.Thread):
    def __init__(self, host_port, on_message_callback):
        threading.Thread.__init__ (self, name="zmq.subscription.%s" % host_port)
        self.daemon = True
        self._host_port = host_port
        self._on_message_callback = on_message_callback
        self._close = False
        self._logger = logging.getLogger('interact.zmqsubscription.' + host_port)
        self._socket = None

    def run(self):
        context = get_zmq().Context()
        self._socket = context.socket(zmq.SUB)
        self._socket.setsockopt(zmq.SUBSCRIBE, "")
        endpoint = 'tcp://' + self._host_port
        self._socket.connect(endpoint)
        self._logger.info('connected to ' + endpoint)
        poll = get_zmq().Poller()
        poll.register(self._socket, zmq.POLLIN)
        while not self._close:
            sockets = dict(poll.poll(POLL_TIMEOUT))
            if self._socket in sockets:
                if sockets[self._socket] == zmq.POLLIN:
                    data = self._socket.recv()
                    self._on_message_callback(data)
        self._logger.info('disconnected from ' + endpoint)

    def close(self):
        if not self._close:
            self._close = True
            time.sleep(POLL_TIMEOUT / 1000.0)
            try:
                self._socket.close()
            except:
                pass


'''
Wrapper to enable mocking
'''
_zmq_ref = [zmq]
def get_zmq(): return _zmq_ref[0]
def set_zmq(ref): _zmq_ref[0] = ref
