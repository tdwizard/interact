'''
Created on Aug 22, 2011

@author: jonas
'''
import zookeeper
import threading
import logging

ZOO_OPEN_ACL_UNSAFE = {"perms":0x1f, "scheme":"world", "id" :"anyone"};


class ZkSubscribe(object):
    def __init__(self, quorum, service_name, added_callback, removed_callback, connect_timeout = 30):
        self._quorum = quorum
        self._service_name = service_name
        self._added_callback = added_callback
        self._removed_callback = removed_callback
        self._connect_timeout = connect_timeout
        self._logger = logging.getLogger('interact.zksubscribe.' + service_name)
        self._leaves = None
        self._client_leaf = None
        self._connected = False
        self._zk_handle = None

    def start(self):
        (connected, zk_handle) = _connect(self._quorum, self._connect_timeout, self._logger)
        self._connected = connected
        if not connected:
            raise Exception('Unable to connect to zookeeper')            
        self._zk_handle = zk_handle
        client_node = _ensure_client_node(self._zk_handle, self._service_name)
        created = False
        # add self as client in zookeeper tree
        while not created:
            nr_clients = len(_zk_get_children(self._zk_handle, client_node, None))
            self._client_leaf = client_node + ('/subscriber%s' % nr_clients)
            created = _create_leaf(self._zk_handle, self._client_leaf, '')
            if created:
                self._logger.info('Client registered: ' + self._client_leaf)

        server_node = _ensure_server_node(self._zk_handle, self._service_name)
        def watcher(zk_handle, type, state, path):
            self._logger.debug("watcher: %s, %s, %s, %s" % (zk_handle, type, state, path))
            current_leaves = set(_zk_get_children(self._zk_handle, server_node, watcher))
            common_leaves = self._leaves.intersection(current_leaves)
            added_leaves = current_leaves.difference(common_leaves)
            removed_leaves = self._leaves.difference(common_leaves)
            self._leaves = current_leaves
            added = list(added_leaves)
            removed = list(removed_leaves)
            self._logger.info('Service list update, added: %s, removed: %s' % (added, removed))
            if added:
                self._added_callback(added)
            if removed:
                self._removed_callback(removed)
        self._leaves = set(_zk_get_children(self._zk_handle, server_node, watcher))
        leaves = list(self._leaves)
        self._logger.info('initial services: %s' % leaves)
        if leaves:
            self._added_callback(leaves)

    def stop(self):
        if self._connected:
            try:
                _zk_delete(self._zk_handle, self._client_leaf)
            except Exception as e:
                self._logger.warn('Error deleting %s, %s' % (self._client_leaf, e))
            try:
                _zk_close(self._zk_handle)
            except Exception as e:
                self._logger.warn('Error closing zookeeper, %s' % e)
            self._logger.info('Stopped listening')
            self._connected = False
        else:
            self._logger.info('Not unregistered since not connected to zookeeper')


class ZkPublish(object):
    def __init__(self, quorum, service_name, host_port, connect_timeout = 30):
        self._quorum = quorum
        self._service_name = service_name
        self._host_port = host_port
        self._connect_timeout = connect_timeout
        self._logger = logging.getLogger('interact.zkpublish.' + service_name)
        self._connected = False
        self._server_leaf = None
        self._zk_handle = None

    def start(self):
        (connected, zk_handle) = _connect(self._quorum, self._connect_timeout, self._logger)
        self._connected = connected
        if not connected:
            raise Exception('Unable to connect to zookeeper')
        self._zk_handle = zk_handle
        server_node = _ensure_server_node(self._zk_handle, self._service_name)
        self._server_leaf = server_node + '/' + self._host_port
        created = _create_leaf(self._zk_handle, self._server_leaf, '')
        if created:
            self._logger.info('Service registered: ' + self._server_leaf)
        else:
            self._logger.warn('Service node %s already present, restarting after crash?' % self._server_leaf)
            _zk_delete(self._zk_handle, self._server_leaf)
            _create_leaf(self._zk_handle, self._server_leaf, '')

    def get_nr_subscribers(self):
        client_node = _get_client_node(self._service_name)
        return len(_zk_get_children(self._zk_handle, client_node, None))

    def stop(self):
        if self._connected:
            try:
                _zk_delete(self._zk_handle, self._server_leaf)
            except Exception as e:
                self._logger.warn('Error deleting %s, %s' % (self._server_leaf, e))
            try:
                _zk_close(self._zk_handle)
            except Exception as e:
                self._logger.warn('Error closing zookeeper, %s' % e)
            self._logger.info('Service unregistered')
            self._connected = False
        else:
            self._logger.info('Not unregistered since not connected to zookeeper')


def _connect(quorum, timeout_seconds, logger):
    zk_handle = None
    connected_wrapper = [False]
    condition = threading.Condition()
    def connected_callback(zk_handle, type, state, path):
        condition.acquire()
        try:
            connected_wrapper[0] = True
            condition.notify()
        finally:
            condition.release()

    try:
        condition.acquire()
        zk_handle = _zk_init(quorum, connected_callback, timeout_seconds * 1000)
        condition.wait(timeout_seconds)
        if not connected_wrapper[0]:
            logger.warn('Unable to connect to the ZooKeeper cluster, quorum=%s' % quorum)
            raise IOError
    finally:
        condition.release()
    logger.info('Connected')
    return (connected_wrapper[0], zk_handle)


def _ensure_service_node(zk_handle, service_name):
    _create_ignore(zk_handle, '/services')
    service_node = '/services/' + service_name
    _create_ignore(zk_handle, service_node)
    return service_node

def _get_client_node(service_name):
    return '/services/' + service_name + '/client'

def _ensure_server_node(zk_handle, service_name):
    service_node = _ensure_service_node(zk_handle, service_name)
    server_node = service_node + '/server'
    _create_ignore(zk_handle, server_node)
    return server_node

def _ensure_client_node(zk_handle, service_name):
    service_node = _ensure_service_node(zk_handle, service_name)
    client_node = service_node + '/client'
    _create_ignore(zk_handle, client_node)
    return client_node

def _create_ignore(zk_handle, node):
    try:
        _zk_create(zk_handle, node, '', [ZOO_OPEN_ACL_UNSAFE], 0)
    except zookeeper.NodeExistsException:
        pass

def _create_leaf(zk_handle, node, data):
    try:
        _zk_create(zk_handle, node, data, [ZOO_OPEN_ACL_UNSAFE], zookeeper.EPHEMERAL)
        return True
    except zookeeper.NodeExistsException:
        return False


'''
Wrapper to enable mocking
'''
_zk_ref = [zookeeper]
def get_zk(): return _zk_ref[0]
def set_zk(ref): _zk_ref[0] = ref

'''
Wrapper methods for naive reconnect handling (one retry)
'''        
def _zk_create(zk_handle, node, data, acl, mode):
    try:
        get_zk().create(zk_handle, node, data, acl, mode)
    except (zookeeper.ConnectionLossException, zookeeper.OperationTimeoutException):
        get_zk().create(zk_handle, node, data, acl, mode)

def _zk_close(zk_handle):
    try:
        get_zk().close(zk_handle)
    except (zookeeper.ConnectionLossException, zookeeper.OperationTimeoutException):
        get_zk().close(zk_handle)

def _zk_get_children(zk_handle, service_node, watcher):
    try:
        return get_zk().get_children(zk_handle, service_node, watcher)
    except (zookeeper.ConnectionLossException, zookeeper.OperationTimeoutException):
        return get_zk().get_children(zk_handle, service_node, watcher)

def _zk_delete(zk_handle, service_leaf):
    try:
        # synch delete don't work from atexit...
        get_zk().adelete(zk_handle, service_leaf)
    except (zookeeper.ConnectionLossException, zookeeper.OperationTimeoutException):
        get_zk().adelete(zk_handle, service_leaf)

def _zk_init(quorum, connected_callback, timeout):
    try:
        return get_zk().init(quorum, connected_callback, timeout)
    except (zookeeper.ConnectionLossException, zookeeper.OperationTimeoutException):
        return get_zk().init(quorum, connected_callback, timeout)


