'''
Created on Aug 24, 2011

@author: jonas
'''

class RequestServiceContext(object):
    def __init__(self, service_name, zk_host_port = 'localhost:2181', request_schema = None, response_schema = None):
        self.service_name = service_name
        self.zk_host_port = zk_host_port
        self.request_schema = request_schema
        self.response_schema = response_schema

class ResponseServiceContext(object):
    def __init__(self, service_name, service_host_port, zk_host_port = 'localhost:2181', request_schema = None, response_schema = None):
        self.service_name = service_name
        self.service_host_port = service_host_port
        self.zk_host_port = zk_host_port
        self.request_schema = request_schema
        self.response_schema = response_schema

class SubscriptionServiceContext(object):
    def __init__(self, service_name, zk_host_port = 'localhost:2181', notification_schema = None):
        self.service_name = service_name
        self.zk_host_port = zk_host_port
        self.notification_schema = notification_schema

class PublishServiceContext(object):
    def __init__(self, service_name, service_host_port, zk_host_port = 'localhost:2181', notification_schema = None):
        self.service_name = service_name
        self.service_host_port = service_host_port
        self.zk_host_port = zk_host_port
        self.notification_schema = notification_schema
