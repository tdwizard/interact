import gevent
from gevent_zookeeper import ZookeeperFramework, MonitorListener
from zookeeper import NodeExistsException
import logging


class Listener(MonitorListener):
    def __init__(self, added_callback, removed_callback, logger):
        self._added_callback = added_callback
        self._removed_callback = removed_callback
        self._logger = logger

    def created(self, path, item):
        self._added_callback([path])

    def modified(self, path, item):
        self._logger.warn("Got unexpected modify notification for %s" % path)

    def deleted(self, path):
        self._removed_callback([path])

class ZkSubscribe(object):
    def __init__(self, quorum, service_name, added_callback, removed_callback, connect_timeout = 30):
        self._quorum = quorum
        self._service_name = service_name
        self._connect_timeout = connect_timeout
        self._logger = logging.getLogger('interact.zksubscribe.' + service_name)
        self._framework = None
        self._listener = Listener(added_callback, removed_callback, self._logger)

    def start(self):
        def run():
            path = '/services/%s/server' % self._service_name
            self._framework = ZookeeperFramework(self._quorum, self._connect_timeout)
            self._framework.connect()
            # make sure service node exists
            try:
                self._framework.create().parents_if_needed().for_path(path)
                self._logger.debug('Made sure %s exists' % path)
            except Exception as e:
                pass
            self._framework.monitor().children().using(self._listener).for_path(path)
            self._logger.debug('Subscribed to %s' % path)
        gevent.spawn(run)

    def stop(self):
        self._framework.close()

class ZkPublish(object):
    def __init__(self, quorum, service_name, host_port, connect_timeout = 30):
        self._quorum = quorum
        self._service_name = service_name
        self._host_port = host_port
        self._connect_timeout = connect_timeout
        self._logger = logging.getLogger('interact.zkpublish.' + service_name)
        self._server_leaf = None
        self._framework = None

    def start(self):
        def run():
            self._framework = ZookeeperFramework(self._quorum, self._connect_timeout)
            self._framework.connect()
            path = '/services/%s/server/%s' % (self._service_name, self._host_port)
            nr_loops = 30
            for x in range(nr_loops):
                try:
                    self._framework.create().parents_if_needed().as_ephemeral().for_path(path)
                    break;
                except NodeExistsException:
                    if x < nr_loops:
                        gevent.sleep(1)
                    else:
                        raise
            self._logger.debug('Created %s' % path)
        gevent.spawn(run)

    def stop(self):
        self._framework.close()

