import interact
if interact.USE_GEVENT:
    import gevent
    import gevent.monkey
#    import gevent_zeromq
    gevent.monkey.patch_all()
#    gevent_zeromq.monkey_patch()
    import zk_gevent_helper
    zk = zk_gevent_helper
    def create_pool():
        from gevent.pool import Pool
        return Pool(size=interact.GEVENT_POOL_SIZE)
    print 'running gevent interact'
else:
    import zk_helper
    zk = zk_helper
    def create_pool():
        from multiprocessing import Pool
        return Pool(processes=interact.THREAD_POOL_SIZE)
    print 'running vanilla interact'
import zmq_helper
zmq = zmq_helper

import json_helper
import logging
import Queue
import time




'''
TODO: Add possibility to explicitly add/remove services to be able to handle the case where the zk integration fails
TODO: Add api version as multipart msg
TODO: reduce response_timeout with the time we waited in queue.get()!
TODO: Add possibility to see if all interested clients have connected
TODO: add custom server selector in send, e.g. based on hash of an additional argument (instead of always round robin, enabler for server side caching)
'''

class RequestService(object):

    def __init__(self, context, missing_response_callback = None):
        '''
        context must be of type interact.context.RequestServiceContext.
        '''
        self.started = False
        self._context = context
        self._missing_response_callback = missing_response_callback
        self._json_validator = json_helper.JsonValidator(
                request_schema = context.request_schema, 
                response_schema = context.response_schema)
        self._service_queue = Queue.Queue()
        self._added_services = dict()
        self._pending_removed_services = dict()
        self._zk_subscriber = None
        self._logger = logging.getLogger('interact.requestservice.' + context.service_name)

    def start(self):
        self._zk_subscriber = zk.ZkSubscribe(
                self._context.zk_host_port, 
                self._context.service_name, 
                self._services_added, 
                self._services_removed)
        self._zk_subscriber.start()
        self._logger.info('subscribed to ' + self._context.service_name + ' at ' + self._context.zk_host_port)
        self.started = True

    def stop(self):
        if self.started:
            self.started = False
            while not self._service_queue.empty():
                try:
                    self._service_queue.get().close()
                except Exception as e:
                    self._logger.warn('Error occurred while removing service %s' % e)
            try:
                self._zk_subscriber.stop()
            except Exception as e:
                self._logger.warn('Error occurred while disconnecting from zookeeper %s' % e)
            self._logger.info('stopped')

    def request(self, request, on_response, on_timeout, response_timeout = 30):
        '''
        Calling this method from multiple threads is allowed.
        on_response/on_timeout is called from a different thread with the response as parameter.
        '''
        if not self.started:
            raise Exception('Not started yet')
        self._json_validator.validate_request(request)
        def validating_on_response(response, on_response=on_response):
            self._json_validator.validate_response(response)
            on_response(response)
        service = self._get_service(on_timeout, response_timeout)
        if service is None:
            return

        # Get rid of pending removed services from queue
        while service in self._pending_removed_services:
            service_endpoint = self._pending_removed_services.pop(service)
            try:
                service.close()
                self._logger.info('closed ' + service_endpoint)
            except Exception as e:
                self._logger.warn('Error occurred while removing service %s: %s' % (service_endpoint, e))
            service = self._get_service(on_timeout, response_timeout)
            if service is None:
                return
        try:
            service.request(request, validating_on_response, on_timeout, response_timeout)
        finally:
            self._service_queue.put(service)

    def _get_service(self, on_timeout, timeout):
        service = None
        try:
            service = self._service_queue.get(timeout=timeout)
        except Queue.Empty:
            self._logger.warn('Service not available, timed out getting reference to %s' % self._context.service_name)
            on_timeout()
        return service

    def available(self):
        '''
        Check to see if there is a remote service connected
        '''
        if not self.started:
            raise Exception('Not started yet')
        return len(self._added_services) > 0

    def _services_added(self, new_services):
        for service_endpoint in new_services:
            try:
                service = zmq.ZmqRequestService(service_endpoint, missing_response_callback = self._missing_response_callback)
                if interact.USE_GEVENT: # je comprend de rien, pourquoui ne c'est pas fonctionet avec service.start() a la case gevent?
                    import gevent # yes, doing it again...to make it run plain service.start if it doesnt exist
                    gevent.spawn(service.start)
                else:
                    service.start()
                self._added_services[service_endpoint] = service
                self._service_queue.put(service)
                self._logger.info('added ' + service_endpoint)
            except Exception as e:
                self._logger.error('Error occurred while initializing service %s: %s' % (service_endpoint, e))
                self._logger.exception(e)

    def _services_removed(self, removed_services):
        for service_endpoint in removed_services:
            if not self._added_services.has_key(service_endpoint):
                self._logger.warn('error removing service, %s not registered' % service_endpoint)
                continue
            service = self._added_services.pop(service_endpoint)
            self._pending_removed_services[service] = service_endpoint
            self._logger.info('removed ' + service_endpoint)


class ResponseService(object):

    def __init__(self, context, on_request_callback):
        '''
        context must be of type interact.context.ResponseServiceContext.
        on_request_callback is called when a request arrives, the first parameter is the request, 
        the second parameter is the response callback which takes the response as parameter. 
        '''
        self._context = context
        self._on_request_callback = on_request_callback
        self._zmq = None
        self._zk_publisher = None
        self._json_validator = json_helper.JsonValidator(
                request_schema = context.request_schema, 
                response_schema = context.response_schema)
        self._pool = create_pool()
        self._logger = logging.getLogger('interact.responseservice.' + context.service_name + '-' + context.service_host_port)

    def start(self, blocking=False):
        def validating_on_request_callback(request, on_response_callback):
            self._json_validator.validate_request(request)
            def validating_on_response_callback(response, on_response_callback=on_response_callback):
                self._json_validator.validate_response(response)
                on_response_callback(response)
            self._pool.apply_async(self._on_request_callback, (request, validating_on_response_callback))
            #self._on_request_callback(request, validating_on_response_callback)

        self._zk_publisher = zk.ZkPublish(self._context.zk_host_port, self._context.service_name, self._context.service_host_port)
        self._zk_publisher.start()
        self._logger.info('published ' + self._context.service_name + '-' +  self._context.service_host_port + ' at ' + self._context.zk_host_port)
        self._zmq = zmq.ZmqResponseService(self._context.service_host_port, validating_on_request_callback)
        if blocking:
            self._zmq.run()
        else:
            self._zmq.start()

    def stop(self):
        try:
            self._zk_publisher.stop()
        except Exception as e:
            self._logger.warn('Error occurred while removing service from zk %s' % e)
        # just a short grace period
        time.sleep(0.3)
        try:
            self._zmq.close()
        except Exception as e:
            self._logger.warn('Error occurred while closing service %s' % e)
        self._logger.info('stopped')


class PublishService(object):

    def __init__(self, context):
        '''
        context must be of type interact.context.PublishServiceContext.
        '''
        self.started = False
        self._context = context
        self._zmq = None
        self._zk_publisher = None
        self._json_validator = json_helper.JsonValidator(
                notification_schema = context.notification_schema)
        self._logger = logging.getLogger('interact.responseservice.' + context.service_name + '-' + context.service_host_port)

    def start(self):
        self._zmq = zmq.ZmqPublishService(self._context.service_host_port)
        self._zmq.start()
        self._zk_publisher = zk.ZkPublish(self._context.zk_host_port, self._context.service_name, self._context.service_host_port)
        self._zk_publisher.start()
        self._logger.info('published ' + self._context.service_name + '-' +  self._context.service_host_port + ' at ' + self._context.zk_host_port)
        self.started = True

    def publish(self, notification):
        '''
        Thread safe method.
        '''
        if not self.started:
            raise Exception('Not started yet')
        self._json_validator.validate_notification(notification)
        self._zmq.publish(notification)

    def stop(self):
        if self.started:
            self.started = False
            try:
                self._zk_publisher.stop()
            except Exception as e:
                self._logger.warn('Error occurred while removing service from zk %s' % e)
            # just a short grace period
            time.sleep(1)
            try:
                self._zmq.close()
            except Exception as e:
                self._logger.warn('Error occurred while closing service %s: %s' % (self._context.service_host_port, e))
            self._logger.info('stopped')


class SubscriptionService(object):

    def __init__(self, context, on_notification_callback):
        '''
        context must be of type interact.context.SubscriptionServiceContext.
        on_notification_callback is called when a notification arrives, the only parameter is the
        notification data. 
        '''
        self._context = context
        self._on_notification_callback = on_notification_callback
        self._added_services = dict()
        self._json_validator = json_helper.JsonValidator(
                notification_schema = context.notification_schema)
        self._zk_subscriber = None
        self._logger = logging.getLogger('interact.subscriptionservice.' + context.service_name)

    def start(self):
        self._zk_subscriber = zk.ZkSubscribe(
                self._context.zk_host_port, 
                self._context.service_name, 
                self._services_added, 
                self._services_removed)
        self._zk_subscriber.start()
        self._logger.info('subscribed to ' + self._context.service_name + ' at ' + self._context.zk_host_port)

    def _services_added(self, new_services):
        def validating_callback(notification):
            self._json_validator.validate_notification(notification)
            self._on_notification_callback(notification)
        for service_endpoint in new_services:
            try:
                service = zmq.ZmqSubscriptionService(service_endpoint, validating_callback)
                service.start()
                self._added_services[service_endpoint] = service
                self._logger.info('added ' + service_endpoint)
            except Exception as e:
                self._logger.warn('Error occurred while initializing service %s: %s' % (service_endpoint, e))

    def _services_removed(self, removed_services):
        for service_endpoint in removed_services:
            if not self._added_services.has_key(service_endpoint):
                self._logger.warn('error removing service, %s not registered' % service_endpoint)
                continue
            service = self._added_services.pop(service_endpoint)
            try:
                service.close()
            except Exception as e:
                self._logger.warn('Error occurred while closing service %s: %s' % (service_endpoint, e))
            self._logger.info('removed ' + service_endpoint)

    def available(self):
        '''
        Check to see if there is a remote service connected
        '''
        return len(self._added_services) > 0

    def stop(self):
        for (service_endpoint, service) in self._added_services.items():
            try:
                service.close()
            except Exception as e:
                self._logger.warn('Error occurred while closing service %s: %s' % (service_endpoint, e))
        try:
            self._zk_subscriber.stop()
        except Exception as e:
            self._logger.warn('Error occurred while disconnecting from zookeeper %s' % e)
        self._logger.info('stopped')

