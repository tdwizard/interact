import simplejson
import validictory
import logging

class JsonValidator(object):
    def __init__(self, request_schema = None, response_schema = None, notification_schema = None):
        self._request_schema = simplejson.loads(request_schema) if request_schema else None
        self._response_schema = simplejson.loads(response_schema) if response_schema else None
        self._notification_schema = simplejson.loads(notification_schema) if notification_schema else None
        self._logger = logging.getLogger('interact.jsonvalidator')

    def validate_request(self, request):
        if self._request_schema:
            data = simplejson.loads(request)
            try:
                validictory.validate(data, self._request_schema)
            except validictory.validator.ValidationError as e:
                self._logger.error('%s did not validate %s' % (self._request_schema, request))
                raise e

    def validate_response(self, response):
        if self._response_schema:
            data = simplejson.loads(response)
            try:
                validictory.validate(data, self._response_schema)
            except validictory.validator.ValidationError as e:
                self._logger.error('%s did not validate %s' % (self._response_schema, response))
                raise e

    def validate_notification(self, notification):
        if self._notification_schema:
            data = simplejson.loads(notification)
            try:
                validictory.validate(data, self._notification_schema)
            except validictory.validator.ValidationError as e:
                self._logger.error('%s did not validate %s' % (self._notification_schema, notification))
                raise e
